<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/customer">
        <application>
            <first_name><xsl:value-of select="fname"/></first_name>
            <last_name><xsl:value-of select="lname"/></last_name>
            <phone1><xsl:value-of select="substring(phone, 1, 3)"/></phone1>
            <phone2><xsl:value-of select="substring(phone, 5, 3)"/></phone2>
            <phone3><xsl:value-of select="substring(phone, 9, 4)"/></phone3>
            <dob>
                <xsl:value-of select="concat(dob_d, '/', dob_m, '/', dob_y)"/>
            </dob>
            <email_domain>
                <xsl:value-of select="substring-after(email, '@')" />
            </email_domain>
            <status>
                <xsl:choose>
                    <xsl:when test="active = 'TRUE'">Active</xsl:when>
                    <xsl:when test="active = 'FALSE'">Inactive</xsl:when>
                </xsl:choose>
            </status>
            <xsl:if test="not(normalize-space(type)='')">
                <repeat><xsl:value-of select="substring(type, 1, 1)"/></repeat>
            </xsl:if>
        </application>
    </xsl:template>

</xsl:stylesheet>


