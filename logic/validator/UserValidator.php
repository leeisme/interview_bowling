<?php


class UserValidator
{
    public function validate($username, $password)
    {
        try {

            // (Real app typically would use a factory to get a database connection.)
            $db = new PDO('mysql:host=localhost;dbname=the_database;', 'dbuser', 'dbpass');
            $sql = 'SELECT username, passcode FROM users WHERE username = :uname LIMIT 1';
            $query = $db->prepare($sql);
            $query->execute(['uname' => $username]);
            $row = $query->fetch();

            // Check that row was returned
            if (!$row) {
                return false;
            }

            // Additional checks, maybe check if user is active, suspended, etc.

            // Verify password
            $hash = CONFIG::get('dont_do_things_half_hash');  // or whatever secure way to get hash salt.

            $pass = $row['password'];

            return password_verify($pass, $hash);

        } catch (Exception $e) {

            error_log("Database connection could not be established: ". $e->getMessage(), 0);

        }
    }
}