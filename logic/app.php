<?php

require_once 'Player.php';
require_once 'Game.php';


// Create a player to use.
$player = new Player('Player1');

// Create the game.
$game = new Game($player);

// Run the game.
$game->runGame();
