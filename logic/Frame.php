<?php

require_once 'Output.php';

class Frame
{
    const TYPE_PLAYING = -1;
    const TYPE_OPEN = 0;
    const TYPE_SPARE = 1;
    const TYPE_STRIKE = 2;
    const TYPE_ACTIVE = 3;

    /** @var  Integer Current score. */
    private $score;

    /** @var  Integer Frame type: open, spare, strike. */
    public $type;

    /** @var  Integer Frame number */
    public $number;

    /** @var Array Rolls containing the results. */
    public $rolls;

    function __construct($num)
    {
        $this->type = Frame::TYPE_PLAYING;

        // Initialize the rolls.
        $this->rolls = [];

        $this->number = $num;
    }


    /** Is the frame a spare or strike? */
    public function isSpecial()
    {
        return $this->type === Frame::TYPE_STRIKE || $this->type === Frame::TYPE_SPARE;
    }


    /** Returns the number of pins down for this particular frame. */
    public function pinsDown()
    {
        $total = 0;

        foreach ($this->rolls as $roll) {
            $total += $roll['pins'];
        }

        return $total;
    }


    /** Add a roll performed by player. */
    public function addRoll($pins)
    {
        $out = Output::getInstance();

        $currentRoll = count($this->rolls);

        if ($currentRoll === 2) {
            throw new \OutOfBoundsException;
        }

        if ($this->type === Frame::TYPE_STRIKE) {
            throw new Exception('Strike was already bowled.  Cannot add another roll');
        }

        // Keep the roll data
        $newRoll = array(
            "pins" => $pins,
            "strike" => false,
            "spare" => false
        );

        // Determine if this frame is a spare or strike.
        if ($currentRoll === 0 && $pins === 10) {
            $out->prnln("Strike! X");
            $this->type = Frame::TYPE_STRIKE;
            $newRoll['spare'] = true;
        } else if ($currentRoll === 1 && $pins === (10 - $this->pinsDown())) {
            $out->prnln("Spare! /");
            $this->type = Frame::TYPE_SPARE;
            $newRoll['strike'] = true;
        }

        if ($this->type === Frame::TYPE_STRIKE || $this->type === Frame::TYPE_SPARE) {
            $this->score = 10;
        }

        $this->rolls[] = $newRoll;

    }

    /** Return number of rolls. */
    public function rollCount()
    {
        return count($this->rolls);
    }

    /** Getter for score. */
    public function getScore()
    {
        return $this->score;
    }

    /** Setter for score. */
    public function setScore($aScore)
    {
        $this->score = $aScore;
    }
}