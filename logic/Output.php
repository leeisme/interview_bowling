<?php

class Output
{
    // Singleton
    private static $instance;


    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }


    public function prnln($text, $linefeeds = 1)
    {
        echo $text. str_repeat(PHP_EOL, $linefeeds);
    }

    // Protected methods

    protected function __construct()
    {
        // fall through
    }


    protected function __clone()
    {
        // fall through
    }

    protected function __wakeup()
    {
        // fall through
    }

}