<?php

/* Note: Preferable to use auto loaded namespaces */
require_once 'Player.php';
require_once 'Frame.php';
require_once 'Output.php';

class Game
{

    const BONUS = 10;

    /** @var  Frame */
    private $currentFrame;

    /** @var  Array of Frame Objects */
    private $frames;

    /** @var  Player */
    private $player;

    function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * Getter for previousFrame.
     * @return Frame
     */
    private function prevFrame()
    {
        // Always pointing to last frame in array as the "current" frame.
        if ($this->frameCount() > 1) {
            return $this->frames[count($this->frames) - 2];
        }

        return false;
    }


    /** Bowl a single frame with one or two rolls (throws) depending on first roll result. */
    private function bowlFrame()
    {

        // Bowl the first roll.
        $this->roll();

        $frame = $this->currentFrame;

        // If first roll is strike, no need for second roll unless it is the 11th frame
        // and resulting from a strike on frame 10.
        if ($frame->type !== Frame::TYPE_STRIKE ||
            ($this->prevFrame()->type === Frame::TYPE_STRIKE && $this->curFrame()->number === 11)) {
            $this->roll();
        }

        // If frame type was not strike or spare, assign open status.
        if ($this->currentFrame->type === Frame::TYPE_PLAYING) {
            $this->currentFrame->type = Frame::TYPE_OPEN;
        }

        $out = Output::getInstance();
        $out->prnln("Frame Score: ". $this->curFrame()->getScore());
        $out->prnln("--> Frame End:     ". ($this->currentFrame->number));
    }


    /** Roll the bowling ball once. */
    private function roll()
    {
        // Get a valid current frame to work with, determine number of pins to make available
        // and then call addRoll with a random number of pins that were knocked down.

        $frame = $this->currentFrame;

        if (!$frame || $frame->type !== Frame::TYPE_PLAYING) {
            $frame = $this->addFrame();
        }

        // How many pins are "in play" to be knocked down randomly
        if ($frame->getScore() > 0 && count($frame->rolls) > 0) {
            $maxPins = (10 - $frame->pinsDown());
        } else {
            $maxPins = 10;
        }

        $pins = mt_rand(0, $maxPins);

        $frame->addRoll($pins);

        $out = Output::getInstance();
        $out->prnln($pins. ' Pins knocked down');

        // Calculate this frame and if necessary, the previous frame.
        $this->calculateAfterRoll();
    }

    /** Called after each roll.  Update the frame and any previous frame if the previous
     *  frame was a spare or strike
     */
    private function calculateAfterRoll()
    {
        $out = Output::getInstance();

        $prevFrame = $this->prevFrame();

        // Strike or Spare of previous frame is calulated here.
        if ($prevFrame && $prevFrame->isSpecial()) {

            // Strike or Spare?
            if ($prevFrame->type === Frame::TYPE_STRIKE) {
                // Add 10 + this frame's pins for BOTH rolls.
                $prevFrame->setScore(Game::BONUS + $this->curFrame()->pinsDown());
            } else if ($prevFrame->type === Frame::TYPE_SPARE && $this->curFrame()->rollCount() === 1) {
                // Add 10 + this frame pins only for first roll.
                $prevFrame->setScore(Game::BONUS + $this->curFrame()->pinsDown());
            }
        }

        // Calculate current frame if not strike or spare. Otherwise, wait until next frame.
        $out->prnln("This frame type: ". $this->curFrame()->type);

        if (!$this->curFrame()->isSpecial()) {
            $this->curFrame()->setScore($this->curFrame()->pinsDown());
        } else {
            $out->prnln("Special frame bowled!");
        }

    }


    /** Return whether or not there are frames that have not been bowled. */
    private function hasEmptyFrame()
    {
        $frame = $this->getFrame();
        if (!$frame) {
            return false;
        }

        return (count($frame->rolls) === 0);
    }

    /**
     * @return Frame
     */
    private function getFrame()
    {
        if (count($this->frames) === 0) {
            return false;
        }

        $frame = $this->frames[count($this->frames) - 1];

        if ($frame->type !== Frame::TYPE_PLAYING && $frame->type !== Frame::TYPE_SPARE) {
            return false;
        }

        return $frame;
    }


    /** Add a frame to the current game. */
    private function addFrame()
    {
        if ($this->hasEmptyFrame()) {
            throw new Exception('Cannot add another frame when there is an existing un-bowled frame.');
        }

        $frame = new Frame(count($this->frames) + 1);
        $frame->type = Frame::TYPE_PLAYING;
        $this->frames[] = $frame;

        $this->currentFrame = $frame;

        $out = Output::getInstance();
        $out->prnln(PHP_EOL. "--> Start Frame:   $frame->number");

        return $frame;
    }


    /** Getter for current frame. */
    public function curFrame()
    {
        return $this->currentFrame;
    }


    /** Getter for number of frames. */
    public function frameCount()
    {
        return count($this->frames);
    }


    /** Return score for the game. */
    public function getScore()
    {
        $total = 0;

        foreach ($this->frames as $frame) {
            $total += $frame->getScore();
        }

        return $total;
    }

    /** Run an entire 10 frame game including extra frame if bonus frame bowled on 10th frame. */
    public function runGame() {
        // Free existing array and any items.
        $this->frames = [];

        for($i = 1; $i < 11; $i++) {
            $this->bowlFrame();
        }

        // Account for a spare or strike on last (10th) frame.
        if ($this->curFrame()->isSpecial()) {
            $this->bowlFrame();
        }

        // Print out the result of all frames and the game itself.

        $out = Output::getInstance();

        $out->prnln("", 1); // whitespace from any above content on console.

        $out->prnln("--------------------------------------------------");
        $out->prnln("Summary for Player: ". $this->player->name);
        $out->prnln("Total Score: ". $this->getScore());
        $out->prnln("--------------------------------------------------");


        foreach ($this->frames as $frame) {
            // Title
            $output = "Frame: $frame->number,  score: ". $frame->getScore();
            // Roll 1
            $output .= ", R1: ". $frame->rolls[0]['pins'];
            // Roll 2?
            if ($frame->rollCount() > 1) {
                $output .= ", R2: ". $frame->rolls[1]['pins'];
            } else {
                $output .= ", R2: x";
            }
            // Frame type
            if ($frame->type === Frame::TYPE_STRIKE) {
                $output .= ", [STRIKE]";
            } else if ($frame->type === Frame::TYPE_SPARE) {
                $output .= ", [SPARE]";
            } else
                $output .= ", [OPEN]";

            $out->prnln($output);

        }

        // one last whitespace line feed to separate from other output on console.
        $out->prnln("");
    }

}
